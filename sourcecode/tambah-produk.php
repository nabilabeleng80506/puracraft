<?php 
 
 session_start();
    include 'db.php';
    if($_SESSION['username'] == ""){
        header("location : login.php");
    }

include 'db.php';
 
error_reporting(0);
 
session_start();




    if (isset($_POST['submit'])){
        
        // print_r($_FILES['gambar']);
        // menampung inputan dari form
        $kategori   = $_POST['kategori'];
        $nama       = $_POST['nama'];
        $harga      = $_POST['harga'];
        $deskripsi  = $_POST['deskripsi'];
        $status     = $_POST['status'];


        //menampung data file yang diupload
        $filename = $_FILES['gambar']['name'];
        $tmp_name = $_FILES['gambar']['tmp_name'];

        $type1 = explode('.', $filename);
        $type2 = $type1[1];

        $newname = 'produk' .time().'.'.$type2;

        //menampung data format file yang diizinkan
        $tipe_diizinkan = array('JPG', 'JPEG', 'PNG', 'gif');

        //validasi format file
        if(in_array($type2, $tipe_diizinkan)){

            echo '<script>alert("Format file tidak diizinkan")</script>';

        }else{

            move_uploaded_file($tmp_name, './produk/'.$newname);

            $insert = mysqli_query($conn, "INSERT INTO tb_product VALUES (
                null,
                '".$kategori."',
                '".$nama."',
                '".$harga."',
                '".$deskripsi."',
                '".$newname."',
                '".$status."',
                null
                    ) ");
            if($insert){
                echo '<script>alert("Tambah data berhasil")</script>';
                echo '<script>window.location="data-produk.php"</script>';
            }else{
                echo 'gagal'.mysqli_error($conn);
            }
        }

        //proses upload file sekaligus insert ke database
    
    }
    
             


?>

                

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PuraCraft</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
</head>
<body>
    <!-- header -->
    <header>
        <div class="container">
        <h1><a href="dashboard.php">PuraCraft</a></h1>
        <ul>
            <li><a href="dashboard.php">Dashboard</a></h1>
            <li><a href="profile.php">Profile</a></li>
            <li><a href="data-kategori.php">Data Kategori</a></li>
            <li><a href="data-produk.php">Data Produk</a></li>
            <li><a href="keluar.php">Logout</a></li>
        </ul>
    </header>

    <!-- content -->
    <div class="section">
        <div class="container">
            
                <h2>Tambah Data Produk</h2>
                 <div class="box">
                <form action="" method="POST" enctype="multipart/form-data">
                <select class="input-control" name="produk" required>
                    <option value="">--Pilih--</option>
                    <?php 
                        $kategori = mysqli_query($conn, "SELECT * FROM tb_category ORDER BY category_id DESC");
                            while($r = mysqli_fetch_array($kategori)){
                                ?>
                                   <option value="<?php echo $r['category_id'] ?>"><?php echo $r['category_name'] ?></option> 

                            <?php } ?>

                  
                </select>

                
                <input type="text" name="nama" class="input-control" placeholder="Nama Produk" required>
                <input type="text" name="harga" class="input-control" placeholder="Harga" required>
                <input type="file" name="gambar" class="input-control" required>
                <textarea class="input-control" name="deskripsi" placeholder="Deskripsi"></textarea>
                <select class="input-control" name="status">
                    <option value="">--Pilih--</option>
                    <option value="1">Aktif</option>
                    <option value="0">Tidak Aktif</option>
                </select>
                <button name="submit" class="btn">Submit</button>
            </div>
        </form>
            
       
        </div>
    </div>

<!-- footer -->
    <footer>
        <div class="container">
            <small>Copyright &copy; 2023 = PuraCraft.</small>

</body>
</html>