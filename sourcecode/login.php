<?php 
 
include 'db.php';
 
error_reporting(0);
 
session_start();
 
if (isset($_SESSION['username'])) {
    header("Location: dashboard.php");
}
 
if (isset($_POST['submit'])) {
    $username = $_POST['username'];
    $password = md5($_POST['password']);
 
    $sql = "SELECT * FROM tb_admin WHERE username ='$username' AND password='$password'";
    $result = mysqli_query($conn, $sql);
    if ($result->num_rows > 0) {
        $row = mysqli_fetch_assoc($result);
        $_SESSION['username'] = $row['username'];
        header("Location: dashboard.php");
    } else {
        echo "<script>alert('username atau password Anda salah. Silahkan coba lagi!')</script>";
    }
}
 
?>
 
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
 
    <title>Login | PuraCraft</title>
</head>
<body id = "bg-login">
    <div class="alert alert-warning" role="alert">
        <?php echo $_SESSION['error']?>
    </div>
 
    
        <div class="box-login">
        <h2>Login</h2>
        <form action="" method="POST" >
           
                <input type="username" placeholder="Username" name="username" class="input-control" value="<?php echo $_POST['password']; ?>" required>
                
                
                <input type="password" placeholder="Password" name="password" class="input-control" value="<?php echo $_POST['password']; ?>" required>
                
                <button name="submit" class="btn">Login</button>
            
            <p>Anda belum punya akun? <a href="register.php">Register</a></p>
        </form>
    
    </div>
</body>
</html>