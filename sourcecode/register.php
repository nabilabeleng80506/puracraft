<?php 
 
include 'db.php';
 
error_reporting(0);
 
session_start();
 
if (isset($_SESSION['username'])) {
    header("Location: index.php");
}
 
if (isset($_POST['submit'])) {
    $nama = $_POST['admin_name'];
    $username = $_POST['username'];
    $telp = $_POST['admin_telp'];
    $alamat = $_POST['admin_address'];
    $email = $_POST['email'];
    $password = md5($_POST['password']);
    $cpassword = md5($_POST['cpassword']);
 
    if ($password == $cpassword) {
        $sql = "SELECT * FROM tb_admin WHERE admin_email='$email'";
        $result = mysqli_query($conn, $sql);
        if (!$result->num_rows > 0) {
            $sql = "INSERT INTO tb_admin (admin_name, admin_telp, admin_address, username, admin_email, password)
                    VALUES ('$nama', '$telp', '$alamat', '$username', '$email', '$password')";
            $result = mysqli_query($conn, $sql);
            if ($result) {
                echo "<script>alert('Selamat, registrasi berhasil!')</script>";
                $nama = "";
                $telp = "";
                $alamat = "";
                $username = "";
                $email = "";
                $_POST['password'] = "";
                $_POST['cpassword'] = "";
            } else {
                echo "<script>alert('Woops! Terjadi kesalahan.')</script>";
            }
        } else {
            echo "<script>alert('Woops! Email Sudah Terdaftar.')</script>";
        }
         
    } else {
        echo "<script>alert('Password Tidak Sesuai')</script>";
    }
}
 
?>
 
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
 
    <title>Login | PuraCraft</title>
</head>
<body id = "bg-login">
    <div class="box-login">
        <h2>Register</h2>
        <form action="" method="POST" >
                <input type="text" placeholder="Nama" name="admin_name" class="input-control" value="<?php echo $nama; ?>" required>
            
                <input type="text" placeholder="Username" name="username" class="input-control" value="<?php echo $username; ?>" required>

                <input type="email" placeholder="Email" name="email" class="input-control" value="<?php echo $email; ?>" required>
            
                <input type="text" placeholder="Telepon" name="admin_telp" class="input-control" value="<?php echo $telp; ?>" required>
            
                <input type="text" placeholder="Alamat" name="admin_address" class="input-control" value="<?php echo $alamat; ?>" required>
            
                <input type="password" placeholder="Password" name="password" class="input-control" value="<?php echo $_POST['password']; ?>" required>
            
                <input type="password" placeholder="Confirm Password" name="cpassword" class="input-control" value="<?php echo $_POST['cpassword']; ?>" required>
            
                <button name="submit" class="btn">Register</button>
            
            <p class="login-register-text">Anda sudah punya akun? <a href="login.php">Login </a></p>
        </form>
        </div>
    </div>
</body>
</html>