-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2023 at 11:57 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bukatoko`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `admin_telp` varchar(20) NOT NULL,
  `admin_email` varchar(50) NOT NULL,
  `admin_address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`admin_id`, `admin_name`, `username`, `password`, `admin_telp`, `admin_email`, `admin_address`) VALUES
(1, 'Neneng Fajriyati Mukolang', 'Neneng', 'b9ed5397f16c073452002a92c4dcc496', '082146687078', 'nengfajriyati@gmail.com', 'Abila, desa Pura Utara, kec.Pulau Pura, kab.Alor'),
(2, 'Syafira Mutiara', 'Syafira', 'd57d8d5422365e4295153b987f907c5e', '081246687078', 'syafiram@gmail.com', 'Tanjung, kec.Pulau Ende, kab.Ende'),
(3, 'Sasya Putri A.Beleng', 'Sasya', '6341fd65fc1d7b26ddbcb4bee3a14ffa', '082236324567', 'nadara@gmail.com', 'Kalabahi, Kec.Teluk Mutiara, Kab.Alor'),
(4, 'Nurlaila Pirlana M.T. Taneo', 'Laila', 'ed2ab2c2952b781fef13e05bf3309a88', '081245678963', 'nurlaila@gmail.com', 'Lasiana, Kota Kupang'),
(6, 'efsrg', 'rt', 'ff1ccf57e98c817df1efcd9fe44a8aeb', '081237959527', 'syafiramakn@gmail.com', 'Daja, kec.Keo Tengah, kab.Nagekeo');

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE `tb_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`category_id`, `category_name`) VALUES
(1, 'Rajutan'),
(2, 'Tenunan'),
(23, 'Anyaman Bambu');

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE `tb_product` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_description` text NOT NULL,
  `product_image` varchar(100) NOT NULL,
  `product_status` tinyint(1) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_product`
--

INSERT INTO `tb_product` (`product_id`, `category_id`, `product_name`, `product_price`, `product_description`, `product_image`, `product_status`, `date_created`) VALUES
(1, 0, 'Kain sarung', 500, 'Motif Gajah', 'produk1686949924.jpg', 1, '2023-06-16 21:12:04'),
(2, 0, 'Kotak Penyimpanan', 50, 'Original', 'produk1686949982.jfif', 1, '2023-06-16 21:13:02'),
(3, 0, 'Bakul Kecil', 50, 'Original', 'produk1686950040.jpg', 1, '2023-06-16 21:14:00'),
(4, 0, 'Rak', 150, 'Model Wadah', 'produk1686950159.jpg', 1, '2023-06-16 21:50:14'),
(6, 0, 'Kain sarung', 500, 'Motif Naga', 'produk1686951855.jpg', 1, '2023-06-16 21:44:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tb_category`
--
ALTER TABLE `tb_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `category_id` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_category`
--
ALTER TABLE `tb_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tb_product`
--
ALTER TABLE `tb_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
